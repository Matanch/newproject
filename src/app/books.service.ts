import { AngularFirestoreModule, AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  booksService:any = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]

  userCollection: AngularFirestoreCollection = this.db.collection('users');
  bookCollection: AngularFirestoreCollection;
  
    constructor(private db:AngularFirestore,private authservice:AuthService) { }

     getBooks(){
      const booksObservable = new Observable( observer => { setInterval( () => observer.next(this.booksService),5000) } )
      return booksObservable;
     }

    
    //  addBooks(){ 
    //    this.booksService.push({title:'Toy Story', author:'Matan chover'}) 
    //            }

      //  getBooks_From_Firebase():Observable<any[]>{
      //    return this.db.collection('books').valueChanges({idField:'id'});
      //  }

      getBook_from_user_firebase(id:string, userId:string):Observable<any>{
        return this.db.doc(`users/${userId}/books/${id}`).get()
      }
      
      getBooks_From_Firebase(userId:string):Observable<any>{
      //return this.db.collection('books').valueChanges({idField:'id'});
      this.bookCollection = this.db.collection(`users/${userId}/books`);
      return this.bookCollection.snapshotChanges().pipe(
        map(
          collection => collection.map(
            document => {
              const data = document.payload.doc.data();
              data.id = document.payload.doc.id;
              return data;
            }
          )

        )
      )
    }
      // addBooks_To_Firebase(userId:string, title:string, author:string){
      //   const book = {title:title, author:author}
      //   //this.db.collection('books').add(book);
      //   this.userCollection.doc(userId).collection('books').add(book);
      // }

      addBooks_To_Firebase(userId:string, title:string, author:string){
        const book = {title:title, author:author}
        //this.db.collection('books').add(book);
        this.userCollection.doc(userId).collection('books').add(book);
      }
      
      deleteBook(id:string, userId:string){
        this.db.doc(`users/${userId}/books/${id}`).delete();
      }
    

      updateBook(userId:string, id:string, title:string, author:string){
        this.db.doc(`users/${userId}/books/${id}`).update({
          title:title,
          author:author
        })
      }
}
