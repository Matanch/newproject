import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {

  ListOfAuthorService:any = [{id:1, name:'Lewis Carrol'},{id:2, name:'Leo Tolstoy'}, {id:3, name:'Thomas Mann'}]    
  
getListAuthor(){
  const authorObservable = new Observable( observer => { setInterval( () => observer.next(this.ListOfAuthorService),4000) } )
  return authorObservable;
}

addAuthor(new_author:string){
  this.ListOfAuthorService.push({name:new_author})
  console.log(this.ListOfAuthorService)
}
  constructor() { }
}
