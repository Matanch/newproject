import { Component, OnInit } from '@angular/core';
import {BooksService} from './../books.service'
import { Router , ActivatedRoute} from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-bookform',
  templateUrl: './bookform.component.html',
  styleUrls: ['./bookform.component.css']
})
export class BookformComponent implements OnInit {

  title:string;
  author:string;
  id:string;
  isEdit:boolean = false;
  buttonText:string = "Add Book"
  userId: string;

  constructor(private booksService:BooksService,private router:Router,private route:ActivatedRoute,private authservice:AuthService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authservice.user.subscribe(
      user=> {
        this.userId = user.uid;
        //relevant only for update book
        if(this.id){
          this.isEdit = true;
          this.buttonText = "Update Book";
          this.booksService.getBooks_From_Firebase(this.userId).subscribe(
            book => {
              this.author = book.data().author;
              this.title = book.data().title;
            })
        }
      }
    )
  }



  // onSubmitAddBook(){
  //   console.log("submit works")
  //   this.booksService.addBooks_To_Firebase(this.title,this.author);
  //   this.router.navigate(['/books']);
  // }
  onSubmitAddBook(){
    if(this.isEdit){
      this.booksService.updateBook(this.userId,this.id, this.title, this.author);
    }
    else{
      this.booksService.addBooks_To_Firebase(this.userId, this.title, this.author)
    }
   // this.booksService.addBook(this.title, this.author)
    this.router.navigate(['/books']);
  }

}

