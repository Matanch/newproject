import { Component, OnInit } from '@angular/core'
import {BooksService} from './../books.service'
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

/**
 * @title Basic expansion panel
 */
@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  //books:object[] = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]
  panelOpenState = false;
  books$:Observable<any[]>;
  id:string;
  userId:string
  constructor(private booksService:BooksService,public authService:AuthService) { }

  ngOnInit() {


   //this.booksService.getBooks_From_Firebase(this.id).subscribe( (DBbooks)=>this.books$=DBbooks);
   //this.books$ = this.booksService.getBooks_From_Firebase(this.id);
   console.log("NgOnInit started")  
    this.authService.user.subscribe(
     user => {
       this.userId = user.uid;
       this.books$ = this.booksService.getBooks_From_Firebase(this.userId);
      }
    )
  }

  deleteBook(id:string){
    this.booksService.deleteBook(id, this.userId);
  }


}





