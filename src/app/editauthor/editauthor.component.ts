import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-editauthor',
  templateUrl: './editauthor.component.html',
  styleUrls: ['./editauthor.component.css']
})
export class EditauthorComponent implements OnInit {

  authors_id:number;
  authorsEdit:object;
  // authorid:number;
  // authorName:string;
  constructor(private router: Router , private route: ActivatedRoute) { }

  ngOnInit() {
    this.authorsEdit =  this.route.snapshot.params.upDateAuthors;
    console.log(this.authorsEdit)
    this.authors_id =  this.route.snapshot.params.authorsId;
    // this.authorName = this.route.snapshot.params.authorname;
    // this.authorid = this.route.snapshot.params.id;
   }

   onSubmitEditAuthors(){
    // this.router.navigate(['/authors',this.authorName,this.authorid])
    this.router.navigate(['/authors',this.authorsEdit,this.authors_id])
  }

}
