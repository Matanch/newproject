import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import {AuthorsService} from './../authors.service'
import { Observable } from 'rxjs';
/**
 * @title List with selection
 */

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

  //ListOfAuthors:object[] = [{id:1, name:'Lewis Carrol'},{id:2, name:'Leo Tolstoy'}, {id:3, name:'Thomas Mann'}]    
  ListOfAuthors:any;
  ListOfAuthors$:Observable<any>;
  authorSUBname:string;
  name_from_url:string;
  id_from_url:number;
  new_author:string;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private authorsservice: AuthorsService) { }
  ngOnInit() {
    this.name_from_url = this.route.snapshot.params.authorname;
    console.log(this.name_from_url)
    this.id_from_url = this.route.snapshot.params.id;
    this.ListOfAuthors$ = this.authorsservice.getListAuthor();
  }

  // onSubmit(){
  //   this.router.navigate(['/editauthor',this.authorSUBname]);
  // }

   onSubmit(){
     this.authorsservice.addAuthor(this.new_author);
    console.log(this.authorsservice)
   }

}



