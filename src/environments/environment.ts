// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDQoimYj0jLN2zjnJj0G59I-CxFfNo1_9s",
    authDomain: "newproject-31251.firebaseapp.com",
    databaseURL: "https://newproject-31251.firebaseio.com",
    projectId: "newproject-31251",
    storageBucket: "newproject-31251.appspot.com",
    messagingSenderId: "809176944336",
    appId: "1:809176944336:web:5822fde2ca3b29b75158d5",
    measurementId: "G-6BWWB6NLPS"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
